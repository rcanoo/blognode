function Post(title, text, date) {
	this.post_title = title;
	this.post_text = text;
	this.post_date = date || new Date().toDateString();
};


var express = require("express"),
	app = express();
let exphbs = require('express-handlebars');

let bodyParser = require("body-parser");

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

let posts = [];
posts.push(new Post("Post1", "hdsfasdf"));
posts.push(new Post("Post2", "hdsfasdasdfasdf"));
posts.push(new Post("Post3", "hdsfa44444444444sdf"));
 
app.engine("handlebars", exphbs({defaultLayout: 'posts'}));
app.set("view engine", "handlebars");
app.set("views", "./views/layouts");

app.get("/posts", function(req, res) {
	let posts_temp = [];
	for (let i = posts.length - 1; i >= 0; i--) {
		posts_temp.push(posts[i]);
	} 
	res.render("posts", {posts: posts_temp});
});

app.get("/posts/new", function(req, res) {
	let last_post = [];
	last_post.push(posts[posts.length - 1]);
	res.render("posts", {posts: last_post});
});

app.post("/posts", function(req, res){
	console.log("posted");
	let new_post = JSON.parse(req["query"]["post"]);
	console.log(new_post);
	if (new_post) posts.push(new Post(new_post["post_title"], new_post["post_text"]));
	res.end();
});

app.get("/posts/edit", function(req, res) {
	console.log(req["query"]["post"]);
	let response = req["query"]["post"];
	if (response) {
		let new_post = JSON.parse(response);
		posts[posts.length -1] = new_post;
		res.render("posts", {posts: [new_post]});
	}
	else {
		res.end();
	}
});

app.get("/posts/:id", function(req, res) {
	let id_post = parseInt(req["params"]["id"]);
	console.log(req["params"]);
	if (posts[id_post]) {
		let temp_post = [];
		temp_post.push(posts[id_post]);
		res.render("posts", {posts: temp_post});
	} else {
		res.end("ERROR: Post not found.");
	}
});

app.put("/posts/:id", function(req, res) {
	let id_post = parseInt(req["params"]["id"]);
	let response = req["query"]["post"];
	console.log(req["params"]);
	if (posts[id_post] && response) {
		posts[id_post] = JSON.parse(response);
		res.render("posts", {posts: [posts[id_post]]});
	} else {
		res.end("ERROR: Post not found.");
	}
});

app.del("/posts/:id", function(req, res) {
	let id_post = parseInt(req["params"]["id"]);
	if (posts[id_post]) {
		posts.splice(id_post, 1);
		res.end();
	} else {
		res.end("ERROR: Post not found.");
	}
});

app.listen(3000);

